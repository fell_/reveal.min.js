import express from "express";
import { engine } from "express-handlebars";
import { existsSync } from "fs";
import merge from "lodash.merge";
import { createRequire } from "module";
import { dirname, join } from "path";
import slides from "./slides.js";
import { fileURLToPath } from "url";

const require = createRequire(import.meta.url);
const basedir = dirname(fileURLToPath(import.meta.url));
const app = express();

app.locals.config = merge(
  require("../config.json"),
  (() => {
    if (!existsSync("userConfig.json")) return;

    try {
      return require("../userConfig.json");
    } catch (e) {
      console.error(e);
      return {};
    }
  })(),
);

app.engine(
  "hbs",
  engine({
    extname: ".hbs",
    layoutsDir: join(basedir, "views", "layouts"),
    partialsDir: join(basedir, "views", "partials"),
    defaultLayout: "main",
  }),
);

app.set("views", join(basedir, "views"));
app.set("view engine", "hbs");

app.use("/reveal", express.static("node_modules/reveal.js"));
app.use("/katex", express.static("node_modules/katex"));
app.use("/public", express.static("public"));
app.use("/public/config.json", (_, res) => res.json(app.locals.config));
app.use("/public/slides", express.static(app.locals.config.slides.root));

app.use("/", slides);

app.listen(app.locals.config.server.port, () => {
  console.log(`listening on :::${app.locals.config.server.port}`);
});
