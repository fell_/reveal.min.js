import express from "express";
import { readdirSync, statSync } from "fs";
import { extname, join, sep } from "path";

const router = express.Router();

/**
 * @param root {string}
 * @returns string[]
 */
const getSlides = (root) => {
  return _getSlides(root)
    .map((i) => i.substring(root.length))
    .sort((a, b) => a.split(sep)?.pop().localeCompare(b.split(sep)?.pop()));
};

/**
 * @param dirname {string}
 * @returns string[]
 */
const _getSlides = (dirname) => {
  /** @type {string[]} */
  const files = [];

  readdirSync(dirname)
    .map((entry) => join(dirname, entry))
    .filter((entry) => statSync(entry).isDirectory() || extname(entry) == ".md")
    .forEach((entry) => {
      files.push(...(statSync(entry).isFile() ? [entry] : _getSlides(entry)));
    });

  return files;
};

router.get("/", (req, res) => {
  res.render("slides", {
    slides: getSlides(req.app.locals.config.slides.root),
    config: req.app.locals.config,
  });
});

export default router;
