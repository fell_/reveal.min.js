import "/reveal/dist/reveal.js";
import "/reveal/plugin/highlight/highlight.js";
import "/reveal/plugin/markdown/markdown.js";
import "/reveal/plugin/math/math.js";
import "/reveal/plugin/notes/notes.js";
import "/reveal/plugin/search/search.js";
import "/reveal/plugin/zoom/zoom.js";

/**
 * @param theme {string}
 * @param stylesheets {string[]}
 */
const addStyleSheets = (theme, stylesheets) => {
  const head = document.querySelector("head");
  for (const sheet of [
    ...stylesheets,
    `/reveal/dist/theme/${theme}.css`,
    "/public/styles.css",
  ]) {
    const el = document.createElement("link");
    el.setAttribute("rel", "stylesheet");
    el.setAttribute("href", sheet);
    head.appendChild(el);
  }
};

const config = await fetch("/public/config.json").then((res) => res.json());

addStyleSheets(config.theme, config.stylesheets);

Reveal.initialize({
  ...config.revealConfig,
  plugins: [
    RevealMarkdown,
    RevealHighlight,
    RevealMath.KaTeX,
    RevealNotes,
    RevealSearch,
    RevealZoom,
  ],
});
