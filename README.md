# reveal.min.js
Minimal *reveal.js* configuration for rendering presentations with external
markdown. Powered by [reveal.js](https://revealjs.com),
[express](https://expressjs.com), and [handlebars](https://handlebarsjs.com).

## Features
* few dependencies
* central, single-file configuration (see **Configuration** below)
  * sensible defaults
  * full user control
* automatic slide-deck import (see **Slides** below)

## Usage
### Install
To install required dependencies, run
```bash
$ npm install --omit=dev
```

### Serve
To start a local server, run
```bash
$ npm start
```
Your markdown files will be collected and made available under `localhost:8000`.
The port on which the server listens is configurable.

## Configuration
User configuration can be placed in `userConfig.json`. The contents of this file
will be merged with the default configuration in `config.json`; user
configuration takes precendence over defaults.

Custom CSS may be placed in `public/styles.css`. This is included as a
stylesheet and will **always** be loaded last, to allow overriding any built-in
rules.

The entire *reveal.js* module is exposed under `/reveal` at runtime, to allow
dynamic inclusion of plugins and themes.

Possible configuration keys (not exhaustive):
* `slides`
  * slide-deck root directory and slide separators
* `stylesheets`
  * list of included stylesheets
* `revealConfig`
  * any key/value pair accepted by `Reveal.initialize`

When `stylesheets` is present, none of the default ones will be loaded (apart
from the theme file and `public/styles.css`). In this case, you will need to
include any desired stylesheets, such as *reveal.js*'s `reset.css`, manually.

## Slides
The slide-deck is created by recursively reading all markdown files in a
directory (configurable, default is *slides*, may be relative path outside of
repository). The list of files is flattened and sorted alphabetically, based on
the filename only, such that

```
.
|-- 01.md
|-- 02.txt
|-- 03/
|   |-- 00.md
|   |-- 03a.md
|-- 03.md
|-- 04.md
```

will result in the following slide-deck order:

```
03/00.md
01.md
03.md
03/03a.md
04.md
```

Each file in the resulting list is included as external markdown in the
resulting HTML file (i.e. as its own section element). This means that files
will **always** be horizontally separated. Separation of intra-file slides can
be controlled by configuration variables.

The contents of the deck's root directory are exposed under `/public/slides`
(regardless of the root configuration value) at runtime, to allow dynamic access
to, and inclusion of, images or other files in the presentation. E.g.: a file
located at `./images/file.png` (relative to the root directory) will be
available under `/public/slides/images/file.png`.
